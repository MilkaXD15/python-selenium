import pytest
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import Remote as WebDriver
from selenium.webdriver.common.by import By

from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance() -> WebDriver:
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance) -> WebDriver:
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample2.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_finds_element_by_id(driver: WebDriver):
    element = driver.find_element(By.ID, "paragraphs")


def test_finds_element_by_css_selector(driver: WebDriver):
    element = driver.find_element(By.CSS_SELECTOR, "#app div.md-card-header > div.md-title")


def test_finds_element_by_class_name(driver: WebDriver):
    element = driver.find_element(By.CLASS_NAME, "md-title")


def test_finds_element_by_tag_name(driver: WebDriver):
    element = driver.find_element(By.TAG_NAME, "p")


def test_finds_element_by_link_text(driver: WebDriver):
    element = driver.find_element(By.LINK_TEXT, "Here we go!")


def test_finds_element_by_partial_link_text(driver: WebDriver):
    element = driver.find_element(By.PARTIAL_LINK_TEXT, "we")


def test_finds_element_by_xpath(driver: WebDriver):
    element = driver.find_element(By.XPATH, '//*[@id="app"]')


def test_find_elements_by_css_selector(driver: WebDriver):
    elements = driver.find_elements(By.CSS_SELECTOR, "#app div")


def test_finds_elements_by_class_name(driver: WebDriver):
    elements = driver.find_elements(By.CLASS_NAME, "md-title")


def test_finds_elements_by_tag_name(driver: WebDriver):
    elements = driver.find_elements(By.TAG_NAME, "p")


def test_finds_elements_by_link_text(driver: WebDriver):
    elements = driver.find_elements(By.LINK_TEXT, "Link 1")


def test_finds_elements_by_partial_link_text(driver: WebDriver):
    elements = driver.find_elements(By.PARTIAL_LINK_TEXT, "Link")


def test_finds_elements_by_xpath(driver: WebDriver):
    elements = driver.find_elements(By.XPATH, '//*[@id="app"]//p')


def test_no_such_element_exception(driver: WebDriver):
    try:
        driver.find_element(By.ID, "non_existing_id")
    except NoSuchElementException:
        print("Not found!")


def test_no_elements_found(driver: WebDriver):
    web_elements_empty = driver.find_elements(By.CLASS_NAME, "non_existing_class")
    assert len(web_elements_empty) == 0
