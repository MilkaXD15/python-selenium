from typing import List

import pytest
from selenium.webdriver import Remote as WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance() -> WebDriver:
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance) -> WebDriver:
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample2.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_web_element_properties(driver: WebDriver):
    web_element: WebElement = driver.find_element(By.CSS_SELECTOR, "#buttons button[disabled]")

    assert web_element.is_displayed()
    assert not web_element.is_enabled()
    assert "DISABLED" in web_element.text
    assert "My custom attribute!" in web_element.get_attribute("data-custom-attribute")
    assert "This button is disabled!" == web_element.get_property("title")
    assert "This button is disabled!" == web_element.get_attribute("title")
    assert "inline-block" == web_element.get_property("style")["display"]  # style is a dictionary object
    assert "display: inline-block;" == web_element.get_attribute("style")


def test_find_within_the_web_element(driver: WebDriver):
    ordered_list = driver.find_element(By.TAG_NAME, "ol")
    assert ordered_list.get_attribute("id") == "ordered-list"

    first_list_item = ordered_list.find_element(By.TAG_NAME, "li")  # First list item in this list
    assert first_list_item.text == "List Item 1"

    list_items = ordered_list.find_elements(By.TAG_NAME, "li")  # All list items
    assert len(list_items) == 3

    # ordered_list.screenshot(config.SCREENSHOTS_DIR + "/ordered_list.png")


def test_list_is_not_a_web_element(driver: WebDriver):
    web_elements: List[WebElement] = driver.find_elements(By.CSS_SELECTOR, "p")

    # Won't work - web_elements is a list
    # web_elements.get_property("title")
    assert type(web_elements) is list

    # But in the list, there are web element objects
    assert isinstance(web_elements[0], WebElement)
