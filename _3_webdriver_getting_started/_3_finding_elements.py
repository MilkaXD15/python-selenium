import pytest
from selenium.webdriver import Remote as WebDriver
from selenium.webdriver.common.by import By
from common import config, helpers


# TODO Modify according to TODOs in this file.
#  Open a "tested" webpage in a browser. User inspector to play with it.

@pytest.fixture(scope="module")
def driver_instance() -> WebDriver:
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance) -> WebDriver:
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample2.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_finds_button_by_title(driver: WebDriver):
    # TODO Find disabled button (disabled=disabled, type=button) (find element by css selector)
    button = None
    # TODO Get data-custom-attribute value (get_attribute)
    attr_value = None

    # Assertions. No changes required. Make sure all assertions pass
    assert not button.is_enabled()
    assert button.text == "DISABLED"
    assert "My custom attribute!" == attr_value
    assert button.is_displayed()


def test_verifies_3_subheadings_on_the_page(driver: WebDriver):
    # TODO Find all h1 with class=md-subheading inside div with id=paragraphs (find elements by css selector)
    divs = []

    # Assertions. No changes required. Make sure all assertions pass
    assert len(divs) == 3


def test_verifies_ordered_list_items(driver: WebDriver):
    # TODO Find all list items in ol with id=ordered-list (find element by css selector)
    list_items = None
    # TODO In list_items_texts array store all list item texts
    list_items_texts = []

    # Assertions. No changes required. Make sure all assertions pass
    assert "List Item 1" in list_items_texts
    assert "List Item 2" in list_items_texts
    assert "List Item 3" in list_items_texts


def test_verifies_links_texts(driver: WebDriver):
    # TODO Find div with id=links (find_element_by_id)
    div = None
    # TODO Inside the div object find all links (find elements by tag name)
    links = None
    # TODO In link_texts array store all link texts
    link_texts = []

    # Assertions. No changes required. Make sure all assertions pass
    assert "Here we go!" in link_texts
    assert "Link 1 (clicked: false)" in link_texts
    assert "Link 2 (clicked: false)" in link_texts
    assert "Link 3 (clicked: false)" in link_texts


def test_verifies_names_in_the_table(driver: WebDriver):
    # TODO Find all cells with names (find elements by css selector)
    name_cells = None
    # TODO In names array store all names
    names = []

    # Assertions. No changes required. Make sure all assertions pass
    assert "Shawna Dubbin" in names
    assert "Odette Demageard" in names
    assert "Vera Taleworth" in names
