Test Automation with Python, Pytest and Selenium
================================================

## Prerequisites

- Git, Terminal (e.g. [cmder](https://blog.qalabs.pl/narzedzia/git-cmder/)). Recommended: cmder full on Windows
- Python 3.10+
- [PyCharm](https://blog.qalabs.pl/narzedzia/python-pycharm).
- Up-to-date `Chrome` and `Firefox` browsers (Note: Update the browsers before running tests in this project)

## Setup on Windows

- Open terminal
- Navigate to project directory
- Create and activate virtual environment by running:

```
venv-install.bat
```

- Install dependencies:

```
pip install -r requirements.txt
```

- Verify project setup by running:

```
python -m pytest _0_env_test/env_test.py
```

- Import project to PyCharm

### How to activate the virtual environment after you restart shell?

- Open terminal
- Navigate to project directory and execute:

```
venv/Scripts/activate.bat
```

## Setup on macOS

- Open terminal
- Create and activate virtual environment

```
python -m venv venv
source venv/bin/activate
```

- Install dependencies:

```
pip install -r requirements.txt
```

- Verify project setup by running:

```
python -m pytest _0_env_test/env_test.py
```

- Import project to PyCharm


## Verify project setup in terminal


- Open terminal
- Navigate to project directory
- Activate virtual env for your project
- Execute the following command:

```
python -m pytest _0_env_test/env_test.py
```

- Expected results:
    - Firefox and Chrome tests executed successfully (you should actually see the browsers starting and closing)
    - Screenshots created in `screenshots` directory
    - In the terminal you should see that 3 out of 3 tests passed
  
```
(venv) ➜  python-selenium python -m pytest _0_env_test/env_test.py
================================================= test session starts ==================================================
platform darwin -- Python 3.8.6, pytest-6.2.2, py-1.10.0, pluggy-0.13.1
rootdir: /Users/kolorobot/python-selenium
collected 3 items

_0_env_test/env_test.py ...                                                                                      [100%]

=================================================== warnings summary ===================================================
_0_env_test/env_test.py::test_chrome
  /Users/kolorobot/python-selenium/_0_env_test/env_test.py:45: DeprecationWarning: use options instead of chrome_options
    driver = webdriver.Chrome(

-- Docs: https://docs.pytest.org/en/stable/warnings.html
============================================ 3 passed, 1 warning in 11.10s =============================================
(venv) ➜  python-selenium
```

## Verify project setup in PyCharm

Make sure `pytest` is selected as default test runner before running any test. 

How to do this? 

Open `Settings > Python Integrated Tools` and Select `pytest` in `Testing` section:

![settings](docs/pycharm-settings.png)

- Open the project
- In the project explorer navigate to `_0_env_test`
  
![settings](docs/pycharm-project-view.png)

- Right click on `env_test.py` and select `Run pytest in env_test.py`
  
![settings](docs/pycharm-run-test-with-pytest.png)

- Expected results:
  - All 3 tests passed.


## Troubleshooting

### `PyCharm` not discovering `venv`

The virtual environment previously setup should be automatically detected by `PyCharm`. If for some reason, it was not, you must configure the project SDK manually. Follow the official tutorial on configuring the virtual environment:

https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html

On other issue might be that you have used spaces in the path of your project. Make sure you don't use spaces in the project path.

### Remove run configurations

If you happen to run the test file without `pytest` set as default test runner, you may experience issues when re-running the test. You may need to clean up previously created run configurations.

- Open `Run > Edit Configurations...`
- Remove existing run configurations
- Apply changes



### Manual driver download

The project uses `WebDriverManager` to download the newest drivers (see: https://blog.qalabs.pl/python-selenium/webdriver-manager-pierwsze-kroki/). If WDM is unable to work, you may download the drivers for your OS manually, put them into `drivers` directory and then change the way how driver is initialized in the code.

Instead of:

```python
driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()))
```

use:

```python
driver = webdriver.Firefox()
```

### Upgrading project dependencies

```
pip install pytest
pip install selenium
pip install webdriver-manager
pip freeze > requirements.txt
```


## See also

- Check `common` package to see configuration options