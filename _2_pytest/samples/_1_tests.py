import pytest


# Test method 1 that simply passes
def test_1():
    pass


# Fail the test
def test_2a():
    pytest.fail("Not implemented")


# Test method 2 that fails with exception
def test_2b():
    raise NotImplementedError("Not implemented")


# Assert two strings are equal
def test_3():
    assert "abc" == "abc"


# Assert two strings are equal (fails)
def test_4():
    assert "abc" != "abc"


# Assert two numbers are equal (fails with message)
def test_5():
    assert "abc" != "abc", "'abc' and 'abc' are equal"
